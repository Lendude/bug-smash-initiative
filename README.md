
# Generating graphs

## Obtaining data
Just run parseToCsv.php inside PHPStorm or something and it will give you a csv file containing all open bugs for Drupal core.

## Data visualisation
Open index.html and this will show you a data visualisation of a number of data sets that were collected.

# Generating counts per component

Run parseToCsvCounts. It will generate two csv files in /tmp, one with the counts of bugs per year per component, bsi-bugs-count-[date].csv and the other
a csv of all the issues, bsi-bugs-[date].csv.

# Generating data for two week time period

From the command line `php ./parseToCsvTwoWeeks.php -s 'start date'`. The issue 
statistics for bug reports for the two week period starting on the start date are displayed. For example, these are the  for
the fortnight starting 2021-11-01 the results are;
```
Today: 2021-11-27

From: 2021-01-11 to 2021-01-25

  All bugs
  Open: 25, including 0 fixed
  Closed: 42
    Age   Count
     00   21
     01   03
     02   01
     03   02
     04   04
     05   06
     07   01
     08   03
     10   01

           Status            Count
  closed (duplicate)           13
  closed (won't fix)            1
  closed (works as designed)    3
  closed (cannot reproduce)     9
  closed (fixed)               12
  closed (outdated)             4

  Approximately 110 yr reduction in total number of years of all open bugs.

  Only bugs tagged Bug Smash Initiative
  Open: 3, including 0 fixed
  Closed: 20
    Age   Count
     00   06
     01   01
     02   01
     04   02
     05   05
     07   01
     08   03
     10   01

           Status            Count
  closed (duplicate)            8
  closed (won't fix)            1
  closed (cannot reproduce)     7
  closed (fixed)                1
  closed (outdated)             3

  Approximately 84 yr reduction in total number of years of all open bugs.
```
