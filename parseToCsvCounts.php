<?php

/**
 * @file
 * Produces a csv file containing all open bugs for Drupal core.
 */

ini_set('default_socket_timeout', 1200);
include './versions.php';

$start_time = time();

$firstLineKeys = FALSE;
$file_handle = fopen('/tmp/bsi-bugs-' . date('U') . '.csv', 'w');

/**
 * Here the user can change some parameters of the searches.
 */

/**
 * The issue statuses to get.
 *
 * All status values.
 *  1 = active
 *  2 = fixed
 *  3 = closed (duplicate)
 *  4 = postponed
 *  5 = closed (won't fix)
 *  6 = closed (works as designed)
 *  7 = closed (fixed)
 *  8 = needs review
 *  13 = needs work
 *  14 = reviewed & tested by the community
 *  15 = patch (to be ported)
 *  16 = postponed (maintainer needs more info)
 *  17 = closed (outdated)
 *  18 = closed (cannot reproduce)
 */
$statuses = [
  1,
  4,
  8,
  13,
  14,
  16,
];

// The curl timeout.
$timeout = 1200;

// Select the fields that will be included in the CSV.
$fields = [
  'nid',
  'title',
  'created',
  'last_comment_timestamp',
  'field_issue_component',
  'field_issue_version',
  'field_issue_status',
];

// Set the start time.
$start_time = time();

ini_set('default_socket_timeout', $timeout);

$firstLineKeys = FALSE;
$file_handle = fopen('/tmp/bsi-bugs-' . date('U') . '.csv', 'w');

// Initialize the counters.
$results = [];
$empty_years = [];
$count_header = [];
$count_header[] = 'Component';
for ($i = 0; $i < 20; $i++) {
  $empty_years[$i] = 0;
  $count_header[] = $i;
}
$count_header[] = 'Total';

// Now loop through all the statuses for all versions process the issues.
foreach ($versions as $version) {
  foreach ($statuses as $status) {
    echo 'Starting: ' . $version . ':' . $status;
    // We need to limit this to 10 items per page or the reply will get to big
    // for the Drupal API to parse and it will die with a 5xx error.
    $json_url = 'https://www.drupal.org/api-d7/node.json?limit=10&type=project_issue&field_project=3060&field_issue_status=' . $status . '&field_issue_category=1&field_issue_version=' . $version;
    $json_url .= '&sort=created&direction=DESC';
    do {
      echo '.';
      $json = get_data($json_url, $timeout);

      if ($json === FALSE || empty(json_decode($json, TRUE))) {
        echo 'x';
        // Try once more, if that fails, we bail out.
        $json = file_get_contents($json_url);
        if ($json === FALSE) {
          echo 'X';
          break;
        }
      }
      $array = json_decode($json, TRUE);

      foreach ($array['list'] as $line) {
        foreach ($line as $key => $value) {
          if (!in_array($key, $fields, TRUE)) {
            unset($line[$key]);
          }
        }
        if (empty($firstLineKeys)) {
          $firstLineKeys = array_keys($line);
          array_push($firstLineKeys, 'years');
          fputcsv($file_handle, $firstLineKeys);
          $firstLineKeys = array_flip($firstLineKeys);
        }
        $line_array = [];
        foreach ($line as $value) {
          if (is_array($value)) {
            $value = implode(' ', $value);
          }
          $line_array[] = $value;
        }

        $component = $line['field_issue_component'];
        if (!isset($results[$component])) {
          $results[$component] = $empty_years;
        }
        $diff = $start_time - $line['created'];
        $yrs = (int) ($diff / 31556926);
        $yrs = (string) $yrs;
        $results[$component][$yrs] = isset($results[$component][$yrs]) ? $results[$component][$yrs] + 1 : 1;
        $line_array[] = $yrs;

        fputcsv($file_handle, $line_array);
      }
      $json_url = !empty($array['next']) ? str_replace('/node?', '/node.json?', $array['next']) : FALSE;
    } while (!empty($json_url));

    echo ' Done: ' . $version . ':' . $status . PHP_EOL;
  }
}
// Close the csv of all issues.
fclose($file_handle);

// Create a count file, compute the counts and save as CSV.
$file_handle = fopen('/tmp/bsi-bugs-count' . date('U') . '.csv', 'w');
fputcsv($file_handle, $count_header);
ksort($results);
foreach ($results as $key => $result) {
  $tmp = $result;
  array_unshift($tmp, $key);
  $tmp[] = array_sum($result);
  fputcsv($file_handle, $tmp);
}
fclose($file_handle);
exit;

/**
 * Retrieves data from Drupal.org.
 *
 * @param string $json_url
 *   The url.
 * @param int $timeout
 *   The timeout to use.
 *
 * @return bool|string
 *   The data or false on failure.
 */
function get_data(string $json_url, int $timeout): bool|string {
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $json_url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

  $json = curl_exec($ch);
  curl_close($ch);
  return $json;
}
