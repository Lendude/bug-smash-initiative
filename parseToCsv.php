<?php

/**
 * @file
 * Produces csv files of the bug count per component.
 *
 * This does not count issues that moved to the Drupal 7 issue queue.
 *
 * Usage: php ./parseToCsvTwoWeeks.php -s 'start date'
 */

ini_set('default_socket_timeout', 1200);
include './versions.php';

$firstLineKeys = FALSE;
$file_handle = fopen('/tmp/bsi-bugs-' . date('U') . '.csv', 'w');

$statuses = [
  1,
  8,
  13,
  14,
];
// Set the tid for the "Bug Smash Initiative".
$bug_smash_tid = '194077';
foreach ($versions as $version) {
  foreach ($statuses as $status) {
    echo 'Starting: ' . $version . ':' . $status;
    // We need to limit this to 10 items per page or the reply will get to big
    // for the Drupal API to parse and it will die with a 5xx error.
    $json_url = 'https://www.drupal.org/api-d7/node.json?limit=10&type=project_issue&field_project=3060&field_issue_status=' . $status . '&field_issue_category=1&field_issue_version=' . $version;
    do {
      echo '.';
      $ch = curl_init();
      $timeout = 1200;

      curl_setopt($ch, CURLOPT_URL, $json_url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

      $json = curl_exec($ch);
      curl_close($ch);
      if ($json === FALSE || empty(json_decode($json, TRUE))) {
        echo 'x';
        // Try once more, if that fails, we bail out.
        $json = file_get_contents($json_url);
        if ($json === FALSE) {
          echo 'X';
          break;
        }
      }
      $array = json_decode($json, TRUE);

      $fields = [
        'nid',
        'title',
        'created',
        'last_comment_timestamp',
        'field_issue_version',
        'field_issue_status',
        'field_issue_priority',
        'flag_tracker_follower_count',
      ];
      foreach ($array['list'] as $line) {
        $bug_smash = 0;
        if (is_array($line["taxonomy_vocabulary_9"])) {
          foreach ($line["taxonomy_vocabulary_9"] as $data) {
            if ($data['id'] === $bug_smash_tid) {
              $bug_smash = 1;
              break;
            }
          }
        }
        foreach ($line as $key => $value) {
          if (!in_array($key, $fields, TRUE)) {
            unset($line[$key]);
          }
        }
        if (empty($firstLineKeys)) {
          $firstLineKeys = array_keys($line);
          $firstLineKeys[] = 'Is bugsmashed';
          fputcsv($file_handle, $firstLineKeys);
          $firstLineKeys = array_flip($firstLineKeys);
        }
        $line_array = [];
        foreach ($line as $value) {
          if (is_array($value)) {
            $value = implode(' ', $value);
          }
          $line_array[] = $value;
        }
        $line_array[] = $bug_smash;
        fputcsv($file_handle, $line_array);
      }
      $json_url = !empty($array['next']) ? str_replace('/node?', '/node.json?', $array['next']) : FALSE;
    } while (!empty($json_url));
    echo 'Done: ' . $version . ':' . $status . PHP_EOL;
  }
}
