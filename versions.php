<?php

$versions = [
  '11.x-dev',
  '10.3.x-dev',
  '10.2.x-dev',
  '10.1.x-dev',
  '10.0.x-dev',
  '9.5.x-dev',
  '9.4.x-dev',
  '9.3.x-dev',
  '9.2.x-dev',
  '9.1.x-dev',
  '9.0.x-dev',
];
