<?php

/**
 * @file
 * Computes bugsmash statistics for a two week period.
 *
 * This does not count issues that moved to the Drupal 7 issue queue.
 *
 * Usage: php ./parseToCsvTwoWeeks.php -s 'start date'
 */

ini_set('default_socket_timeout', 1200);
include './versions.php';

// Get the start date.
$usage = "php ./parseToCsvTwoWeeks.php -s 'start date'\n";

if ($argc < 2) {
  echo $usage;
  exit();
}

$date = $argv[2];

$start_time = strtotime($date);
if (!$start_time) {
  echo "Invalid start date.\n";
  exit;
}

$end_time = strtotime('+2 weeks', $start_time);

$firstLineKeys = FALSE;
$file_handle_open = fopen('/tmp/bugs-open' . date('U') . '.csv', 'w');
$file_handle_fixed = fopen('/tmp/bugs-fixed' . date('U') . '.csv', 'w');
$file_handle_closed = fopen('/tmp/bugs-closed' . date('U') . '.csv', 'w');

/**
 * Here the user can change some parameters of the searches.
*/
// A list of versions to search.
$versions = array_merge($versions, [
  '8.9.x-dev',
  '8.8.x-dev',
  '8.7.x-dev',
  '8.6.x-dev',
  '8.5.x-dev',
  '8.4.x-dev',
  '8.3.x-dev',
  '8.2.x-dev',
  '8.1.x-dev',
  '8.0.x-dev',
]);

/**
 * The issue statuses to get.
 *
 * All status values.
 *  1 = active
 *  2 = fixed
 *  3 = closed (duplicate)
 *  4 = postponed
 *  5 = closed (won't fix)
 *  6 = closed (works as designed)
 *  7 = closed (fixed)
 *  8 = needs review
 *  13 = needs work
 *  14 = reviewed & tested by the community
 *  15 = patch (to be ported)
 *  16 = postponed (maintainer needs more info)
 *  17 = closed (outdated)
 *  18 = closed (cannot reproduce)
 */
$statuses = [
  'open' => [
    1 => 'active',
    4 => 'postponed',
    8 => 'needs review',
    13 => 'needs work',
    14 => 'reviewed & tested by the community',
  ],
  'fixed' => [
    2 => 'fixed',
  ],
  'closed' => [
    3 => "closed (duplicate)",
    5 => "closed (won't fix)",
    6 => "closed (works as designed)",
    18 => "closed (cannot reproduce)",
    17 => "closed (outdated)",
    7 => "closed (fixed)",
  ],
];

// The curl timeout.
$timeout = 1200;

// The types or categories for each issue.
$types = ['open', 'fixed', 'closed'];

// Change to TRUE to output the issues as CSV.
$output_csv = FALSE;

// Select the fields that will be included in the CSV.
$fields = [
  'nid',
  'title',
  'created',
  'last_comment_timestamp',
  'field_issue_version',
  'field_issue_status',
];

/**
 * Begin by validating the command line input.
 */
$usage = "php ./parseToCsvTwoWeeks.php -s 'start date'\n";

if ($argc < 2) {
  echo $usage;
  exit();
}

// Get the start date.
$start_time = strtotime($argv[2]);
if (!$start_time) {
  echo "Invalid start date.\n";
  exit;
}

/**
 * Here we initialize the data based on the above values.
 */
ini_set('default_socket_timeout', $timeout);

// Set the end time to 2 weeks after the start time.
$end_time = strtotime('+2 weeks', $start_time);

// Setup some things for each type.
$firstLineKeys = FALSE;
$file_handles = [];
foreach ($types as $type) {
  // Create file handles to use for each issue type.
  if ($output_csv) {
    $file_handles[$type] = fopen('/tmp/bugs-' . $type . date('U') . '.csv', 'w');
  }
  // Create a template for the results array.
  $tmp[$type] = [
    'count' => 0,
    'years' => [],
    'status' => [],
  ];
  $tmp['total_diff'] = 0;
}
// Initialize the results arrays.
$results[0] = ['title' => 'All bugs'] + $tmp;
$results[1] = ['title' => 'Only bugs tagged Bug Smash Initiative'] + $tmp;

// Set the tid for the "Bug Smash Initiative".
$bug_smash_tid = '194077';

// Now loop through all the statuses, versions and types to get and process
// the issues.
foreach ($types as $type) {
  echo 'Starting: ' . strtoupper($type) . PHP_EOL;
  foreach ($versions as $version) {
    echo '  Starting: ' . $version;
    foreach ($statuses[$type] as $status => $status_text) {
      echo '   ' . $status_text;
      // We need to limit this to 10 items per page or the reply will get to big
      // for the Drupal API to parse and it will die with a 5xx error.
      $json_url = 'https://www.drupal.org/api-d7/node.json?limit=10&type=project_issue&field_project=3060&field_issue_status='
        . $status . '&field_issue_category=1&field_issue_version='
        . $version;
      if ($type == 'open') {
        $json_url .= '&sort=created&direction=DESC';
      }
      else {
        $json_url .= '&sort=field_issue_last_status_change&direction=DESC';
      }

      do {
        echo '.';
        $json = get_data($json_url, $timeout);

        if ($json === FALSE || empty(json_decode($json, TRUE))) {
          echo 'x';
          // Try once more, if that fails, we bail out.
          $json = file_get_contents($json_url);
          if ($json === FALSE) {
            echo 'X';
            break;
          }
        }
        $array = json_decode($json, TRUE);

        $stop = FALSE;
        foreach ($array['list'] as $line) {
          // For closed issue process lines where the issue status changed is
          // after or equal to the start time.
          $last_status_change = $line['field_issue_last_status_change'];
          $issue_time = $line['field_issue_last_status_change'];
          // For open issue count issues created in the period.
          if ($type == 'open') {
            $issue_time = $line['created'];
          }
          if ($start_time > $issue_time) {
            $stop = TRUE;
            break 2;
          }
          // Skip the line if after the period.
          if ($issue_time > $end_time) {
            continue;
          }
          // Set bug_smash flag if taxonomy term for 'Bug Smash Initiative' is
          // in the list of tags.
          $bug_smash = FALSE;
          foreach ($line["taxonomy_vocabulary_9"] as $data) {
            if ($data['id'] == $bug_smash_tid) {
              $bug_smash = TRUE;
              break;
            }
          }
          $results[0][$type]['count']++;
          if ($bug_smash) {
            $results[1][$type]['count']++;
          }
          if ($type !== 'open') {
            $results = do_not_open($line, $type, $status_text, $bug_smash, $results);
          }
          if ($output_csv) {
            do_output_csv($file_handles, $line, $fields, $type);
          }
        }
        $json_url = !empty($array['next']) ? str_replace('/node?', '/node.json?', $array['next']) : FALSE;
      } while (!$stop && !empty($json_url));
    }
    echo PHP_EOL . '  Done: ' . $version . PHP_EOL;
  }
  echo 'Done: ' . strtoupper($type) . PHP_EOL;

}

report($start_time, $end_time, $results);
foreach ($file_handles as $handle) {
  fclose($handle);
}
exit();

/**
 * Output the line as CSV.
 *
 * @param resource[] $file_handles
 *   An array of file handles, in the same order as the $type.
 * @param string[] $line
 *   The issue line to output.
 * @param string[] $fields
 *   The field that will be output.
 * @param string $type
 *   The type of the issue.
 */
function do_output_csv(array $file_handles, array $line, array $fields, string $type): void {
  foreach ($line as $key => $value) {
    if (!in_array($key, $fields, TRUE)) {
      unset($line[$key]);
    }
  }
  if (empty($firstLineKeys)) {
    $firstLineKeys = array_keys($line);
    fputcsv($file_handles[$type], $firstLineKeys);
  }
  $line_array = [];
  foreach ($line as $value) {
    if (is_array($value)) {
      $value = implode(' ', $value);
    }
    $line_array[] = $value;
  }
  fputcsv($file_handles[$type], $line_array);
}

/**
 * Process an issue.
 *
 * @param array $line
 *   The issue to process.
 * @param string $type
 *   The type of issue.
 * @param string $status_text
 *   The issue status.
 * @param bool $bug_smash
 *   True if tagged 'Bug Smash Initiative'.
 * @param array $results
 *   The array for results.
 *
 * @return array
 *   The updated results.
 */
function do_not_open(array $line, string $type, string $status_text, bool $bug_smash, array $results): array {
  $diff = (int) ($line['last_comment_timestamp'] - $line['created']);
  $results[0]['total_diff'] += $diff;
  $yrs = (int) ($diff / 31556926);
  $yrs = (string) $yrs;
  $results[0][$type]['years'][$yrs] = isset($results[0][$type]['years'][$yrs]) ? $results[0][$type]['years'][$yrs] + 1 : 1;
  $results[0][$type]['status'][$status_text] = isset($results[0][$type]['status'][$status_text]) ? $results[0][$type]['status'][$status_text] + 1 : 1;
  if ($bug_smash) {
    $results[1]['total_diff'] += $diff;
    $results[1][$type]['years'][$yrs] = isset($results[1][$type]['years'][$yrs]) ? $results[1][$type]['years'][$yrs] + 1 : 1;
    $results[1][$type]['status'][$status_text] = isset($results[1][$type]['status'][$status_text]) ? $results[1][$type]['status'][$status_text] + 1 : 1;
  }
  return $results;
}

/**
 * Retrieves data from Drupal.org.
 *
 * @param string $json_url
 *   The url.
 * @param int $timeout
 *   The timeout to use.
 *
 * @return bool|string
 *   The data or false on failure.
 */
function get_data(string $json_url, int $timeout): bool|string {
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $json_url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

  $json = curl_exec($ch);
  curl_close($ch);
  return $json;
}

/**
 * Display the results.
 *
 * @param int $start_time
 *   The start timestamp.
 * @param int $end_time
 *   The end timestamp.
 * @param array $results
 *   The results array.
 */
function report(int $start_time, int $end_time, array $results) {
  $format = 'Y-m-d';
  $indent = '  ';
  $msg = 'Today: ' . date($format, time());
  printf("\n%s\n\n", $msg);
  $msg = 'From: ' . date($format, $start_time) . ' to ' . date($format, $end_time);
  printf("%s\n", $msg);

  // Display results for all bugs and only those tagged Bug Smash Initiative.
  foreach ($results as $data) {
    printf("\n%s%s\n", $indent, $data['title']);

    $fixed_count = $data['fixed']['count'];
    $total_open_count = $data['open']['count'] + $fixed_count;
    $msg = 'Open: ' . $total_open_count . ", including $fixed_count fixed";
    printf("%s%s\n", $indent, $msg);

    $msg = 'Closed: ' . $data['closed']['count'];
    printf("%s%s\n", $indent, $msg);
    ksort($data['closed']['years']);
    printf("%s%s\n", $indent, '  Age   Count');
    foreach ($data['closed']['years'] as $key => $value) {
      printf("%s   %02d   %02d\n", $indent, $key, $value);
    }

    printf("\n%s%15s%17s\n", $indent, 'Status', 'Count');
    $format = $indent . "%-27s%4d\n";
    foreach ($data['closed']['status'] as $key => $value) {
      printf($format, $key, $value);
    }

    // Display overall result.
    $total_open_count = $data['open']['count'];
    $yrs = (int) ($data['total_diff'] / 31556926);

    if ($yrs >= 0) {
      $change = 'reduction';
    }
    else {
      $change = 'increase';
      $yrs = -$yrs;
    }
    printf("\n%sApproximately %2d yr %s in total number of years of all open bugs.\n", $indent, $yrs, $change);
  }
}
